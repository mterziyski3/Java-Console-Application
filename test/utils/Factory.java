package utils;

import com.telerikacademy.projectTeam08.models.BoardImpl;
import com.telerikacademy.projectTeam08.models.MemberImpl;
import com.telerikacademy.projectTeam08.models.TeamImpl;
import com.telerikacademy.projectTeam08.models.contracts.Board;
import com.telerikacademy.projectTeam08.models.contracts.Member;
import com.telerikacademy.projectTeam08.models.contracts.Team;
import com.telerikacademy.projectTeam08.models.tasks.BugImpl;
import com.telerikacademy.projectTeam08.models.tasks.FeedbackImpl;
import com.telerikacademy.projectTeam08.models.tasks.StoryImpl;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Bug;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Feedback;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Story;
import com.telerikacademy.projectTeam08.models.tasks.enums.*;

import java.util.Collections;

import static utils.TestData.TaskBase.*;

public class Factory {
    public static Feedback createFeedback() {
        return new FeedbackImpl(VALID_ID, VALID_TITLE, VALID_DESCRIPTION, VALID_RATING, StatusFeedbackType.NEW);
    }
    public static Bug createBug() {
        return new BugImpl(VALID_ID, VALID_TITLE, VALID_DESCRIPTION, PriorityType.HIGH, SeverityType.CRITICAL,
                StatusBugType.ACTIVE, "TestAssignee", Collections.singletonList("step1; step2; step3"));
    }

    public static Story createStory() {
        return new StoryImpl(VALID_ID, VALID_TITLE, VALID_DESCRIPTION, PriorityType.HIGH, SizeType.MEDIUM,
                StatusStoryType.INPROGRESS, "TestAssignee");
    }

    public static Team createTeam() {
        return new TeamImpl(VALID_TEAM_NAME);
    }

    public static Board createBoard() {
        return new BoardImpl(VALID_BOARD_NAME);
    }

    public static Member registerMember() {
        return new MemberImpl(VALID_MEMBER_NAME);
    }

}

