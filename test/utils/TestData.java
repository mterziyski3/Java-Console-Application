package utils;

import com.telerikacademy.projectTeam08.models.BoardImpl;
import com.telerikacademy.projectTeam08.models.MemberImpl;
import com.telerikacademy.projectTeam08.models.TeamImpl;

import static com.telerikacademy.projectTeam08.models.tasks.FeedbackImpl.RATING_VALUE_MIN;
import static com.telerikacademy.projectTeam08.models.tasks.TaskBase.DESCRIPTION_MIN_LENGTH;
import static com.telerikacademy.projectTeam08.models.tasks.TaskBase.TITLE_MIN_LENGTH;

public class TestData {

    public static class TaskBase {
        public static final String VALID_TITLE = TestUtilities.initializeStringWithSize(TITLE_MIN_LENGTH + 1);
        public static final String VALID_DESCRIPTION = TestUtilities.initializeStringWithSize(DESCRIPTION_MIN_LENGTH + 1);
        public static final String VALID_TEAM_NAME = "x".repeat(TeamImpl.TEAM_NAME_MIN_LENGTH + 1);

        public static final String VALID_BOARD_NAME = "x".repeat(BoardImpl.BOARD_NAME_MIN_LENGTH + 1);

        public static final String VALID_MEMBER_NAME = "x".repeat(MemberImpl.MEMBER_NAME_MIN_LENGTH + 1);
        public static final int VALID_ID = 1;
        public static final int VALID_RATING = RATING_VALUE_MIN + 1;

        public static final String VALID_STEPS = "step1; step2; step3;";
    }
}
