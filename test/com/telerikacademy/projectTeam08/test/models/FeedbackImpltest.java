package com.telerikacademy.projectTeam08.test.models;


import com.telerikacademy.projectTeam08.models.tasks.FeedbackImpl;
import com.telerikacademy.projectTeam08.models.tasks.TaskBase;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Feedback;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusFeedbackType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static utils.TestData.TaskBase.VALID_DESCRIPTION;
import static utils.TestData.TaskBase.VALID_TITLE;
import static utils.TestUtilities.initializeStringWithSize;

public class FeedbackImpltest {

    @Test
    public void FeedbackImpl_ShouldImplementFeedbackInterface() {
        Feedback feedback = new FeedbackImpl(1,"BugTestTitle", "BugTestDescription",
                10, StatusFeedbackType.NEW);
        // Assert
        Assertions.assertTrue(feedback instanceof Feedback);
    }

    @Test
    public void FeedbackImpl_ShouldImplementTaskInterface() {
        Feedback feedback = new FeedbackImpl(1,"BugTestTitle", "BugTestDescription",
                10, StatusFeedbackType.NEW);
        // Assert
        Assertions.assertTrue(feedback instanceof Task);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {TaskBase.TITLE_MIN_LENGTH - 1, TaskBase.TITLE_MAX_LENGTH + 1})
    public void should_throwException_when_titleLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new FeedbackImpl(01,
                        initializeStringWithSize(testLength),
                        VALID_DESCRIPTION,
                        4,
                        StatusFeedbackType.NEW));
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {TaskBase.DESCRIPTION_MIN_LENGTH - 1, TaskBase.DESCRIPTION_MAX_LENGTH + 1})
    public void should_throwException_when_descriptionLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new FeedbackImpl(01,
                        VALID_TITLE,
                        initializeStringWithSize(testLength),
                        4,
                        StatusFeedbackType.NEW));
    }

    @AfterEach
    public void should_create_Feedback_when_ValidValuesArePassed() {
        // Arrange, Act, Assert
        Assertions.assertDoesNotThrow(() -> new FeedbackImpl(
                1,
                VALID_DESCRIPTION,
                VALID_DESCRIPTION,
                4,
                StatusFeedbackType.NEW));
    }
}
