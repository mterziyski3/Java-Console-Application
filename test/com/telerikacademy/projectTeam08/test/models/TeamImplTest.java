package com.telerikacademy.projectTeam08.test.models;

import com.telerikacademy.projectTeam08.models.TeamImpl;
import com.telerikacademy.projectTeam08.models.contracts.Team;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.telerikacademy.projectTeam08.models.TeamImpl.*;
import static utils.TestData.TaskBase.VALID_TEAM_NAME;
import static utils.TestUtilities.initializeStringWithSize;

public class TeamImplTest {

    @Test
    public void TestImpl_ShouldImplementsTeamInterface() {
        Team team = new TeamImpl(VALID_TEAM_NAME);

        //Assert
        Assertions.assertTrue(team instanceof Team);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {TEAM_NAME_MIN_LENGTH-1, TEAM_NAME_MAX_LENGTH + 1})
    public void should_throwException_when_nameLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new TeamImpl(
                        initializeStringWithSize(testLength)));
    }

    @AfterEach
    public void should_create_Team_when_ValidValuesArePassed() {
        Assertions.assertDoesNotThrow(() -> new TeamImpl(
                VALID_TEAM_NAME));
    }
}
