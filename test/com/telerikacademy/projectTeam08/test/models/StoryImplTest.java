package com.telerikacademy.projectTeam08.test.models;

import com.telerikacademy.projectTeam08.models.tasks.StoryImpl;
import com.telerikacademy.projectTeam08.models.tasks.TaskBase;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Story;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.models.tasks.enums.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static utils.TestData.TaskBase.VALID_DESCRIPTION;
import static utils.TestData.TaskBase.VALID_TITLE;
import static utils.TestUtilities.initializeStringWithSize;

public class StoryImplTest {

    @Test
    public void StoryImpl_ShouldImplementStoryInterface() {
        Story story = new StoryImpl(1,"BugTestTitle", "BugTestDescription",
                PriorityType.HIGH, SizeType.LARGE, StatusStoryType.INPROGRESS, "TestAssignee");
        // Assert
        Assertions.assertTrue(story instanceof Story);
    }

    @Test
    public void StoryImpl_ShouldImplementTaskInterface() {
        Story story = new StoryImpl(1,"BugTestTitle", "BugTestDescription",
                PriorityType.HIGH, SizeType.LARGE, StatusStoryType.INPROGRESS, "TestAssignee");
        // Assert
        Assertions.assertTrue(story instanceof Task);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {TaskBase.TITLE_MIN_LENGTH - 1, TaskBase.TITLE_MAX_LENGTH + 1})
    public void should_throwException_when_titleLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new StoryImpl(01,
                        initializeStringWithSize(testLength),
                        VALID_DESCRIPTION,
                        PriorityType.HIGH,
                        SizeType.MEDIUM,
                        StatusStoryType.DONE,
                        "TestAssignee"));
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {TaskBase.DESCRIPTION_MIN_LENGTH - 1, TaskBase.DESCRIPTION_MAX_LENGTH + 1})
    public void should_throwException_when_descriptionLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new StoryImpl(01,
                        VALID_TITLE,
                        initializeStringWithSize(testLength),
                        PriorityType.HIGH,
                        SizeType.MEDIUM,
                        StatusStoryType.DONE,
                        "TestAssignee"));
    }

    @AfterEach
    public void should_create_Story_when_ValidValuesArePassed() {
        // Arrange, Act, Assert
        Assertions.assertDoesNotThrow(() -> new StoryImpl(
                1,
                VALID_DESCRIPTION,
                VALID_DESCRIPTION,
                PriorityType.HIGH,
                SizeType.MEDIUM,
                StatusStoryType.DONE,
                "TestAssignee"));
    }


}
