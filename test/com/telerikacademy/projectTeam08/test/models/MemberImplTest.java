package com.telerikacademy.projectTeam08.test.models;


import com.telerikacademy.projectTeam08.models.MemberImpl;
import com.telerikacademy.projectTeam08.models.contracts.Member;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import static com.telerikacademy.projectTeam08.models.MemberImpl.*;
import static utils.TestData.TaskBase.VALID_MEMBER_NAME;
import static utils.TestUtilities.initializeStringWithSize;

public class MemberImplTest {

    @Test
    public void MemberImpl_ShouldImplementsMemberInterface() {
        Member member = new MemberImpl(VALID_MEMBER_NAME);

        Assertions.assertTrue(member instanceof Member);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {MEMBER_NAME_MIN_LENGTH - 1, MEMBER_NAME_MAX_LENGTH + 1})
    public void should_throwException_when_nameLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new MemberImpl(
                        initializeStringWithSize(testLength)));
    }

    @AfterEach
    public void should_create_member_when_ValidValuesArePassed() {
        Assertions.assertDoesNotThrow(() -> new MemberImpl(
                VALID_MEMBER_NAME));
    }
}

