package com.telerikacademy.projectTeam08.test.models;

import com.telerikacademy.projectTeam08.models.BoardImpl;
import com.telerikacademy.projectTeam08.models.contracts.Board;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.telerikacademy.projectTeam08.models.BoardImpl.*;
import static utils.TestData.TaskBase.VALID_BOARD_NAME;
import static utils.TestUtilities.initializeStringWithSize;

public class BoardImplTest {

    @Test
    public void BoardImpl_ShouldImplementsBoardInterface() {
        Board board = new BoardImpl(VALID_BOARD_NAME);

        //Assert
        Assertions.assertTrue(board instanceof Board);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {BOARD_NAME_MIN_LENGTH-1, BOARD_NAME_MAX_LENGTH + 1})
    public void should_throwException_when_nameLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BoardImpl(
                        initializeStringWithSize(testLength)));
    }

    @AfterEach
    public void should_create_Board_when_ValidValuesArePassed() {
        Assertions.assertDoesNotThrow(() -> new BoardImpl(
                VALID_BOARD_NAME));
    }
}
