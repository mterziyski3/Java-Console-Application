package com.telerikacademy.projectTeam08.test.models;


import com.telerikacademy.projectTeam08.models.tasks.BugImpl;;
import com.telerikacademy.projectTeam08.models.tasks.TaskBase;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Bug;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.models.tasks.enums.PriorityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.SeverityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusBugType;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import java.util.Collections;

import static utils.TestData.TaskBase.*;
import static utils.TestUtilities.initializeStringWithSize;

public class BugImplTest {

    @Test
    public void BugImpl_ShouldImplementBugInterface() {
        Bug bug = new BugImpl(1,"BugTestTitle", "BugTestDescription",
                PriorityType.HIGH, SeverityType.CRITICAL, StatusBugType.ACTIVE, "TestAssignee", Collections.singletonList(VALID_STEPS));
        // Assert
        Assertions.assertTrue(bug instanceof Bug);
    }

    @Test
    public void BugImpl_ShouldImplementTaskInterface() {
        Bug bug = new BugImpl(1,"BugTestTitle", "BugTestDescription",
                PriorityType.HIGH, SeverityType.CRITICAL, StatusBugType.ACTIVE, "TestAssignee", Collections.singletonList(VALID_STEPS));
        // Assert
        Assertions.assertTrue(bug instanceof Task);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {TaskBase.TITLE_MIN_LENGTH - 1, TaskBase.TITLE_MAX_LENGTH + 1})
    public void should_throwException_when_titleLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BugImpl(01,
                        initializeStringWithSize(testLength),
                        VALID_DESCRIPTION,
                        PriorityType.HIGH,
                        SeverityType.CRITICAL,
                        StatusBugType.ACTIVE,
                        "TestAssignee",
                Collections.singletonList(VALID_STEPS)));
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {TaskBase.DESCRIPTION_MIN_LENGTH - 1, TaskBase.DESCRIPTION_MAX_LENGTH + 1})
    public void should_throwException_when_descriptionLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BugImpl(01,
                        VALID_TITLE,
                        initializeStringWithSize(testLength),
                        PriorityType.HIGH,
                        SeverityType.CRITICAL,
                        StatusBugType.ACTIVE,
                        "TestAssignee",
                        Collections.singletonList(VALID_STEPS)));
    }

    @AfterEach
    public void should_create_Bug_when_ValidValuesArePassed() {
        // Arrange, Act, Assert
        Assertions.assertDoesNotThrow(() -> new BugImpl(
                1,
                VALID_DESCRIPTION,
                VALID_DESCRIPTION,
                PriorityType.HIGH,
                SeverityType.CRITICAL,
                StatusBugType.ACTIVE,
                "TestAssignee",
                Collections.singletonList(VALID_STEPS)));
    }

}


