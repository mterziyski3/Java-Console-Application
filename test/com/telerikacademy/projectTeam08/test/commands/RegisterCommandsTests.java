package com.telerikacademy.projectTeam08.test.commands;

import com.telerikacademy.projectTeam08.commands.contracts.Command;
import com.telerikacademy.projectTeam08.core.CommandFactoryImpl;
import com.telerikacademy.projectTeam08.core.TaskBoardRepositoryImpl;
import com.telerikacademy.projectTeam08.core.contracts.CommandFactory;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.contracts.Member;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static utils.Factory.registerMember;
import static utils.TestData.TaskBase.VALID_MEMBER_NAME;

public class RegisterCommandsTests {
    private TaskBoardRepository taskBoardRepository = new TaskBoardRepositoryImpl();

    private CommandFactory commandFactory = new CommandFactoryImpl();

    @BeforeEach
    public void before() {
        commandFactory = new CommandFactoryImpl();
        taskBoardRepository = new TaskBoardRepositoryImpl();
    }

    @Test
    public void should_registerMember_when_inputIsValid() {
        Command registerMember = commandFactory.createCommandFromCommandName
                ("registerMember", taskBoardRepository);

        Member testMember = registerMember();

        List<String> params = List.of(VALID_MEMBER_NAME);

        registerMember.execute(params);

        Member toCompare = taskBoardRepository.getMembers().get(0);

        Assertions.assertEquals(testMember.getName(), toCompare.getName());
    }

    @Test
    public void should_registerMember_when_argumentsCountIsValid() {
        Command registerMember = commandFactory.createCommandFromCommandName
                ("registerMember", taskBoardRepository);

        List<String> params = List.of(VALID_MEMBER_NAME);

        registerMember.execute(params);

        Assertions.assertEquals(1, taskBoardRepository.getMembers().size());
    }
}
