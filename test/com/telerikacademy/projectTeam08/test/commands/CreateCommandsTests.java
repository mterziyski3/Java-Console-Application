package com.telerikacademy.projectTeam08.test.commands;

import com.telerikacademy.projectTeam08.commands.contracts.Command;
import com.telerikacademy.projectTeam08.core.CommandFactoryImpl;
import com.telerikacademy.projectTeam08.core.TaskBoardRepositoryImpl;
import com.telerikacademy.projectTeam08.core.contracts.CommandFactory;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.contracts.Board;
import com.telerikacademy.projectTeam08.models.contracts.Member;
import com.telerikacademy.projectTeam08.models.contracts.Team;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Bug;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Feedback;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Story;
import com.telerikacademy.projectTeam08.models.tasks.enums.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static utils.Factory.*;
import static utils.TestData.TaskBase.*;
import static utils.TestData.TaskBase.VALID_TEAM_NAME;

public class CreateCommandsTests {

    private TaskBoardRepository taskBoardRepository = new TaskBoardRepositoryImpl();

    private CommandFactory commandFactory = new CommandFactoryImpl();

    @BeforeEach
    public void before() {
        commandFactory = new CommandFactoryImpl();
        taskBoardRepository = new TaskBoardRepositoryImpl();
    }


    @Test
    public void createFeedback_ShouldCreate_WhenInputValid() {
        Command createFeedback = commandFactory.createCommandFromCommandName("createFeedback", taskBoardRepository);
        Feedback testFeedback = createFeedback();
        List<String> params = List.of(VALID_TITLE, VALID_DESCRIPTION, String.valueOf(VALID_RATING), String.valueOf(StatusFeedbackType.NEW));

        createFeedback.execute(params);

        Feedback toCompare = (Feedback) taskBoardRepository.getFeedbacks().get(0);
        Assertions.assertEquals(testFeedback.getTitle(), toCompare.getTitle());
        Assertions.assertEquals(testFeedback.getDescription(), toCompare.getDescription());
    }

    @Test
    public void createBug_ShouldCreate_WhenInputValid() {
        Command createBug = commandFactory.createCommandFromCommandName("createBug", taskBoardRepository);
        Bug testBug = createBug();
        List<String> params = List.of(VALID_TITLE, VALID_DESCRIPTION, String.valueOf(PriorityType.HIGH),
                String.valueOf(SeverityType.CRITICAL), String.valueOf(StatusBugType.ACTIVE), "TestAssignee", "step1; step2; step3");

        createBug.execute(params);

        Bug toCompare = (Bug) taskBoardRepository.getBugs().get(0);
        Assertions.assertEquals(testBug.getTitle(), toCompare.getTitle());
        Assertions.assertEquals(testBug.getDescription(), toCompare.getDescription());
    }

    @Test
    public void createStory_ShouldCreate_WhenInputValid() {
        Command createStory = commandFactory.createCommandFromCommandName("createStory", taskBoardRepository);
        Story testStory = createStory();
        List<String> params = List.of(VALID_TITLE, VALID_DESCRIPTION, String.valueOf(PriorityType.HIGH),
                String.valueOf(SizeType.MEDIUM), String.valueOf(StatusStoryType.DONE), "TestAssignee");

        createStory.execute(params);


        Story toCompare = (Story) taskBoardRepository.getStories().get(0);
        Assertions.assertEquals(testStory.getTitle(), toCompare.getTitle());
        Assertions.assertEquals(testStory.getDescription(), toCompare.getDescription());
    }

    @Test
    public void createTeam_ShouldCreate_WhenInputValid() {
        Command createTeam = commandFactory.createCommandFromCommandName("createTeam", taskBoardRepository);
        Team testTeam = createTeam();
        List<String> params = List.of(VALID_TEAM_NAME);

        createTeam.execute(params);

        Team toCompare = (Team) taskBoardRepository.getTeams().get(0);
        Assertions.assertEquals(testTeam.getName(), toCompare.getName());


        // Act, Assert
        Assertions.assertEquals(1, taskBoardRepository.getTeams().size());
    }


    @Test
    public void should_throwException_when_TeamWithSameNameExists() {

        Command createTeam = commandFactory.createCommandFromCommandName("createTeam", taskBoardRepository);
        // Arrange


        createTeam.execute(List.of(VALID_TEAM_NAME));

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> createTeam.execute(List.of(VALID_TEAM_NAME)));
    }

    @Test
    public void Create_Board_ShouldCreate_WhenInputValid() {
        Command createBoard = commandFactory.createCommandFromCommandName("createBoard", taskBoardRepository);
        Board testBoard = createBoard();
        List<String> params = List.of(VALID_BOARD_NAME);

        createBoard.execute(params);

        Board toCompare = (Board) taskBoardRepository.showBoards().get(0);
        Assertions.assertEquals(testBoard.getName(), toCompare.getName());

        // Act, Assert
        Assertions.assertEquals(1, taskBoardRepository.showBoards().size());
    }

    @Test
    public void should_throwException_when_BoardWithSameNameExists() {

        Command createBoard = commandFactory.createCommandFromCommandName("createBoard", taskBoardRepository);
        // Arrange


        createBoard.execute(List.of(VALID_BOARD_NAME));

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> createBoard.execute(List.of(VALID_BOARD_NAME)));
    }

    @Test
    public void Register_Member_ShouldRegister_WhenInputValid() {
        Command registerMember = commandFactory.createCommandFromCommandName("registerMember", taskBoardRepository);
        Member testMember = registerMember();
        List<String> params = List.of(VALID_MEMBER_NAME);

        registerMember.execute(params);

        Member toCompare = (Member) taskBoardRepository.getMembers().get(0);
        Assertions.assertEquals(testMember.getName(), toCompare.getName());

        // Act, Assert
        Assertions.assertEquals(1, taskBoardRepository.getMembers().size());
    }

    @Test
    public void should_throwException_when_MemberWithSameNameExists() {

        Command createMember = commandFactory.createCommandFromCommandName("registerMember", taskBoardRepository);
        // Arrange


        createMember.execute(List.of(VALID_MEMBER_NAME));

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> createMember.execute(List.of(VALID_MEMBER_NAME)));
    }
}


