package com.telerikacademy.projectTeam08.test.commands;

import com.telerikacademy.projectTeam08.commands.AddCommentCommand;
import com.telerikacademy.projectTeam08.commands.contracts.Command;
import com.telerikacademy.projectTeam08.core.CommandFactoryImpl;
import com.telerikacademy.projectTeam08.core.TaskBoardRepositoryImpl;
import com.telerikacademy.projectTeam08.core.contracts.CommandFactory;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.MemberImpl;
import com.telerikacademy.projectTeam08.models.contracts.Member;
import com.telerikacademy.projectTeam08.models.tasks.CommentImpl;
import com.telerikacademy.projectTeam08.models.tasks.FeedbackImpl;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Comment;
import com.telerikacademy.projectTeam08.models.tasks.enums.PriorityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.SeverityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusBugType;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusFeedbackType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static utils.Factory.registerMember;
import static utils.TestData.TaskBase.*;

public class RemoveCommandsTests {
    private TaskBoardRepository taskBoardRepository = new TaskBoardRepositoryImpl();

    private CommandFactory commandFactory = new CommandFactoryImpl();

    @BeforeEach
    public void before() {
        commandFactory = new CommandFactoryImpl();
        taskBoardRepository = new TaskBoardRepositoryImpl();
    }

    @Test
    public void should_removeComment_when_inputIsValid() {

        // SHOULD BE IMPLEMENTED LATER

        List<String> params = List.of("0", "0", "testPerson");

        Member member = new MemberImpl("testPerson");
        taskBoardRepository.addMember(member);

        Command registerMember = commandFactory.createCommandFromCommandName
                ("removeComment", taskBoardRepository);
    }
}
