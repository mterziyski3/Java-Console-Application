# Team 08 Project




## Project Description

Design and implement a Tasks Management console application.

The application will be used by a small team of developers, who need to keep track of all the tasks, surrounding a software product they are building.


##  Operations

The application must support the following operations:

o Create a new person.

o Show all people.

o Show person's activity.

o Create a new team.

o Show all teams.

o Show team's activity.

o Add person to team.

o Show all team members.

o Create a new board in a team.

o Show all team boards.

o Show board's activity.

o Create a new Bug/Story/Feedback in a board.

o Change the Priority/Severity/Status of a bug.

o Change the Priority/Size/Status of a story.

o Change the Rating/Status of a feedback.

o Assign/Unassign a task to a person.


## Technical Requirements

o Follow the OOP best practices:

o Use data encapsulation.

o Use inheritance and polymorphism properly.

o Use interfaces and abstract classes properly.

o Use static members properly.

o Use enumerations properly.

o Aim for strong cohesion and loose coupling.

o Follow guidelines for writing clean code:

o Proper naming of classes, methods, and fields.

o Small classes and methods.

o Well formatted and consistent code.

o No duplicate code.

o Implement proper user input validation and display meaningful user messages.

o Implement proper exception handling.

o Prefer using the Streaming API, then using ordinary loops, wherever you can.

o Cover the core functionality with unit tests (at least 80% code coverage of the models and commands).

o There is no need to test the printing commands.

o Use Git to keep your source code and for team collaboration.
