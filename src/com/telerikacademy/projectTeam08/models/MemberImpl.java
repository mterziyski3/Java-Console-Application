package com.telerikacademy.projectTeam08.models;

import com.telerikacademy.projectTeam08.models.contracts.Member;
import com.telerikacademy.projectTeam08.models.tasks.EventLog;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Bug;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Comment;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class MemberImpl implements Member {

    private static final String YOU_ARE_NOT_THE_AUTHOR = "You are not the author of the comment you are trying to remove!";
    public static final int MEMBER_NAME_MIN_LENGTH = 5;
    public static final int MEMBER_NAME_MAX_LENGTH = 15;
    public static final String MEMBER_NAME_LENGTH_ERR = "The member name should be between 5 and 15 symbols";
    public static final String COMMENT_REMOVED = "Comment removed: %s";
    public static final String COMMENT_ADDED = "Comment added: %s";
    public static final String NAME_CHANGED_FROM_TO = "Name changed from %s to %s.";
    private String name;

    private final List<Task> tasks;

    private final List <EventLog> activityHistory;

    public MemberImpl(String name) {
        setName(name);
        this.tasks = new ArrayList<>();
        this.activityHistory = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        ValidationHelpers.validateIntRange(name.length(),
                MEMBER_NAME_MIN_LENGTH, MEMBER_NAME_MAX_LENGTH, MEMBER_NAME_LENGTH_ERR);

        if(this.name != null) {
            EventLog log = new EventLog(String.format(NAME_CHANGED_FROM_TO, this.name, name));
            activityHistory.add(log);
        }
        this.name = name;
    }

    @Override
    public List<Task> getTasks() {
        return this.tasks;
    }

    @Override
    public void addComment(Comment commentToAdd, Task taskToAdd) {
        EventLog log = new EventLog(String.format(COMMENT_ADDED, commentToAdd));
        activityHistory.add(log);

        taskToAdd.addComment(commentToAdd);
    }

    @Override
    public void removeComment(Comment commentToRemove, Task taskToRemoveComment) {
        if (commentToRemove.getAuthor().equals(getName())) {
            throw new IllegalArgumentException(YOU_ARE_NOT_THE_AUTHOR);
        } else {
            EventLog log = new EventLog(String.format(COMMENT_REMOVED, commentToRemove));
            activityHistory.add(log);

            taskToRemoveComment.removeComment(commentToRemove);
        }
    }

    @Override
    public List<EventLog> showActivity() {
        return activityHistory;
    }

    //TODO
    @Override
    public List<Bug> getBugs() {
        return null;
    }

    @Override
    public String toString() {
        return String.format("Name: %s", name);
    }
}
