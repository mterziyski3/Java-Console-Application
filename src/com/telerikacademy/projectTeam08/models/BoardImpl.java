package com.telerikacademy.projectTeam08.models;

import com.telerikacademy.projectTeam08.models.contracts.Board;
import com.telerikacademy.projectTeam08.models.tasks.EventLog;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {

    public static final int BOARD_NAME_MIN_LENGTH = 5;
    public static final int BOARD_NAME_MAX_LENGTH = 10;
    public static final String BOARD_NAME_LENGTH_ERR = "The board name should be between 5 and 10 symbols";
    public static final String NEW_TASK_ADDED = "New task added: %s";
    public static final String NEW_TASK_REMOVED = "New task removed: %s";
    private String name;
    private List<Task> tasks;
    private List<EventLog> activityHistory;

    public BoardImpl(String name) {
        setName(name);
        tasks = new ArrayList<>();
        activityHistory = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        ValidationHelpers.validateIntRange(name.length(),
                BOARD_NAME_MIN_LENGTH, BOARD_NAME_MAX_LENGTH, BOARD_NAME_LENGTH_ERR);
        this.name = name;
    }

    @Override
    public void addTask(Task task) {
        tasks.add(task);
        EventLog log = new EventLog(String.format(NEW_TASK_ADDED, task.getTitle()));

        activityHistory.add(log);
    }

    @Override
    public void removeTask(Task task) {
        tasks.add(task);
        EventLog log = new EventLog(String.format(NEW_TASK_REMOVED, task.getTitle()));

        tasks.remove(task);
    }

    @Override
    public List<EventLog> showActivity() {
        return activityHistory;
    }

    @Override
    public String toString() {
        return String.format("Name: %s", getName());
    }
}
