package com.telerikacademy.projectTeam08.models.tasks;

import com.telerikacademy.projectTeam08.models.tasks.contracts.Comment;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;


public class CommentImpl implements Comment {

    public static final int CONTENT_LEN_MIN = 3;
    public static final int CONTENT_LEN_MAX = 500;
    private static final String CONTENT_LEN_ERR = format(
            "Content must be between %d and %d characters long!",
            CONTENT_LEN_MIN,
            CONTENT_LEN_MAX);
    public static final String CONTENT_CHANGED_FROM_TO = "Content changed from %s to %s.";

    private final String author;

    private String content;

    private List<EventLog> activityHistory;

    public CommentImpl(String author, String content) {
        this.author = author;
        this.content = content;

        activityHistory = new ArrayList<>();
    }


    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public String getContent() {
        return content;
    }

    private void setContent(String content) {
        if(this.content != null) {
            EventLog log = new EventLog(String.format(CONTENT_CHANGED_FROM_TO, this.content, content));
            activityHistory.add(log);
        }
        ValidationHelpers.validateIntRange(content.length(), CONTENT_LEN_MIN, CONTENT_LEN_MAX, CONTENT_LEN_ERR);
        this.content = content;
    }
}
