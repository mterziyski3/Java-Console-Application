package com.telerikacademy.projectTeam08.models.tasks.contracts;

public interface Assignable {
    String getAssignee();
}
