package com.telerikacademy.projectTeam08.models.tasks.contracts;

public interface Identifiable {

    int getId();
}
