package com.telerikacademy.projectTeam08.models.tasks.contracts;

import com.telerikacademy.projectTeam08.models.tasks.enums.PriorityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.SeverityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusBugType;

import java.util.List;

public interface Bug extends Task {

    List<String> getSteps();

    SeverityType getSeverityType();

    StatusBugType getStatusBugType();

    void setPriorityType(PriorityType priorityType);

    void setStatusBugType (StatusBugType statusBugType);

    void setSeverityType (SeverityType setSeverityType);

    void addBug(String titleToAdd, String assigneeToAdd);

}
