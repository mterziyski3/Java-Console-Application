package com.telerikacademy.projectTeam08.models.tasks.contracts;

import com.telerikacademy.projectTeam08.models.tasks.enums.StatusFeedbackType;

public interface Feedback extends Task {
    int getRating();

    StatusFeedbackType getStatusFeedbackType();

    void setStatusFeedbackType(StatusFeedbackType statusFeedbackType);

    void setRating(int rating);
}
