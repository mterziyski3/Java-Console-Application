package com.telerikacademy.projectTeam08.models.tasks.contracts;

import com.telerikacademy.projectTeam08.models.tasks.enums.PriorityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.SizeType;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusStoryType;

public interface Story extends Task {

    StatusStoryType getStatusStoryType();

    SizeType getSizeType();

    void setPriorityType(PriorityType priorityType);

    void setSizeType(SizeType sizeType);

    void setStatusStoryType(StatusStoryType statusStoryType);
}
