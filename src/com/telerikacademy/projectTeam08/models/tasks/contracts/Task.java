package com.telerikacademy.projectTeam08.models.tasks.contracts;


import com.telerikacademy.projectTeam08.models.contracts.Printable;
import com.telerikacademy.projectTeam08.models.tasks.EventLog;
import com.telerikacademy.projectTeam08.models.tasks.enums.PriorityType;

import java.util.List;

public interface Task extends Identifiable, Commentable, Printable, Assignable {

    String getTitle();

    String getDescription();

    void addComment(Comment comment);

    void removeComment (Comment comment);

    String getLogs();

//    void assignTask(Task task, String assignee);

    PriorityType getPriorityType();

}
