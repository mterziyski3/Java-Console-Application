package com.telerikacademy.projectTeam08.models.tasks.contracts;

public interface Comment {

     String getAuthor();

     String getContent();

}
