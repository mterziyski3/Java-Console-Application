package com.telerikacademy.projectTeam08.models.tasks.contracts;

import java.util.List;

public interface Commentable {

    List<Comment> getComments();
}
