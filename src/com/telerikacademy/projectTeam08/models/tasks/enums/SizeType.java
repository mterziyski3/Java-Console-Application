package com.telerikacademy.projectTeam08.models.tasks.enums;

public enum SizeType {

    LARGE,
    MEDIUM,
    SMALL
}
