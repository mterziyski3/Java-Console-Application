package com.telerikacademy.projectTeam08.models.tasks.enums;

public enum SeverityType {

    MINOR,
    MAJOR,
    CRITICAL
}
