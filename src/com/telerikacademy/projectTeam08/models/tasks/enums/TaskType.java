package com.telerikacademy.projectTeam08.models.tasks.enums;

public enum TaskType {

    BUG,
    STORY,
    FEEDBACK
}
