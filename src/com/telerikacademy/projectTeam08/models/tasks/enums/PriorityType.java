package com.telerikacademy.projectTeam08.models.tasks.enums;

public enum PriorityType {

    LOW,
    MEDIUM,
    HIGH
}
