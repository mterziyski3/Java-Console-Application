package com.telerikacademy.projectTeam08.models.tasks.enums;

public enum StatusStoryType {

    NOTDONE,
    INPROGRESS,
    DONE
}
