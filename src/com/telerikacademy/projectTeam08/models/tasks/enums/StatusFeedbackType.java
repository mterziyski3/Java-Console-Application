package com.telerikacademy.projectTeam08.models.tasks.enums;

public enum StatusFeedbackType {

    NEW,
    UNSCHEDULED,
    SCHEDULED,
    DONE
}
