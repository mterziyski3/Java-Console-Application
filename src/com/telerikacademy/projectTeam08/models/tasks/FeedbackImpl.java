package com.telerikacademy.projectTeam08.models.tasks;

import com.telerikacademy.projectTeam08.models.tasks.contracts.Feedback;
import com.telerikacademy.projectTeam08.models.tasks.enums.PriorityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusFeedbackType;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

public class FeedbackImpl extends TaskBase implements Feedback {

    public static final int RATING_VALUE_MIN = 0;
    public static final int RATING_VALUE_MAX = 100;
    private static final String RATING_MESSAGE_ERR = "Rating cannot be less than 0 and more than 100!";
    public static final String RATING_CHANGED_FROM_TO = "Rating changed from %s to %s.";
    public static final String NO_ASSIGNEE_FOR_FEEDBACK = "No assignee for Feedback";

    private int rating;
    private StatusFeedbackType statusFeedbackType;

    private PriorityType priorityType = PriorityType.HIGH;


    public FeedbackImpl(int id, String title, String description, int rating, StatusFeedbackType statusFeedbackType) {
        super(id, title, description);
        setRating(rating);
        this.statusFeedbackType = statusFeedbackType;
    }

    @Override
    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        if(this.rating != 0) {
            EventLog log = new EventLog(String.format(RATING_CHANGED_FROM_TO, this.rating, rating));
            historyLog.add(log);
        }
        ValidationHelpers.validateIntRange(rating, RATING_VALUE_MIN, RATING_VALUE_MAX, RATING_MESSAGE_ERR);
        this.rating = rating;
    }

    @Override
    public StatusFeedbackType getStatusFeedbackType() {
        return statusFeedbackType;
    }

    public void setStatusFeedbackType(StatusFeedbackType statusFeedbackType) {
        this.statusFeedbackType = statusFeedbackType;
    }

    @Override
    public String toString() {
        return String.format("Feedback ----%n" +
                "%s" + "Status: %s%n" +
                "Rating: %d", super.toString(), getStatusFeedbackType(), getRating());
    }

    @Override
    public String getAssignee() {
        return NO_ASSIGNEE_FOR_FEEDBACK;
    }

    @Override
    public PriorityType getPriorityType() {
        return this.priorityType;
    }
}
