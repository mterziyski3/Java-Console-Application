package com.telerikacademy.projectTeam08.models.tasks;

import com.telerikacademy.projectTeam08.models.tasks.contracts.Assignable;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Story;
import com.telerikacademy.projectTeam08.models.tasks.enums.PriorityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.SizeType;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusBugType;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusStoryType;

public class StoryImpl extends TaskBase implements Story {

    public static final String PRIORITY_CHANGED_FROM_TO = "Priority changed from %s to %s";
    public static final String SIZE_CHANGED_FROM_TO = "Size changed from %s to %s";
    public static final String STATUS_CHANGED_FROM_TO = "Status changed from %s to %s";
    private PriorityType priorityType;
    private SizeType sizeType;
    private StatusStoryType statusStoryType;
    private String assignee;
    public StoryImpl(int id, String title, String description, PriorityType priorityType, SizeType sizeType, StatusStoryType statusStoryType, String assignee) {
        super(id, title, description);
        setPriorityType(priorityType);
        setSizeType(sizeType);
        setStatusStoryType(statusStoryType);
        setAssignee(assignee);
    }

    @Override
    public String getAssignee() {
        return this.assignee;
    }

    private void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    @Override
    public PriorityType getPriorityType() {
        return priorityType;
    }

    public void setPriorityType(PriorityType priorityType) {

        if(this.priorityType != null) {
            EventLog log = new EventLog(String.format(PRIORITY_CHANGED_FROM_TO, this.priorityType, priorityType));
            historyLog.add(log);
        }

        this.priorityType = priorityType;
    }

    public void setSizeType(SizeType sizeType) {
        if(this.sizeType != null) {
            EventLog log = new EventLog(String.format(SIZE_CHANGED_FROM_TO, this.sizeType, sizeType));
            historyLog.add(log);
        }

        this.sizeType = sizeType;
    }

    public SizeType getSizeType() {
        return sizeType;
    }

    public void setStatusStoryType(StatusStoryType statusStoryType) {
        if(this.statusStoryType != null) {
            EventLog log = new EventLog(String.format(STATUS_CHANGED_FROM_TO, this.statusStoryType, statusStoryType));
            historyLog.add(log);
        }

        this.statusStoryType = statusStoryType;
    }

    public StatusStoryType getStatusStoryType() {
        return statusStoryType;
    }

    @Override
    public String toString() {
        return String.format("Story ----%n" +
                        "%s" + "Priority: %s%n" +
                        "Size: %s%n" + "Status: %s%n",
                super.toString(), getPriorityType(), getSizeType(), getStatusStoryType());
    }
}
