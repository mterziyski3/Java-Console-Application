package com.telerikacademy.projectTeam08.models.tasks;

import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;

import static com.telerikacademy.projectTeam08.utils.ValidationHelpers.*;

import com.telerikacademy.projectTeam08.models.tasks.contracts.Comment;
import java.util.ArrayList;
import java.util.List;

public abstract class TaskBase implements Task {

    public static final int TITLE_MIN_LENGTH = 10;
    public static final int TITLE_MAX_LENGTH = 50;
    public static final String TITLE_ERROR_MESSAGE = "Title should be between 10 and 50 characters!";
    public static final int DESCRIPTION_MIN_LENGTH = 10;
    public static final int DESCRIPTION_MAX_LENGTH = 500;
    public static final String DESCRIPTION_ERROR_MESSAGE = "Description should be between 10 and 500 characters!";
    public static final String DESCRIPTION_CHANGED_FROM_TO = "Description changed from %s to %s";

    private final int id;

    private String title;

    private String description;

    private final List<Comment> comments;

    protected List<EventLog> historyLog;

    public TaskBase(int id, String title, String description) {
        this.id = id;
        setTitle(title);
        setDescription(description);
        this.comments = new ArrayList<>();
        this.historyLog = new ArrayList<>();
    }

    @Override
    public String getTitle() {
        return title;
    }

    private void setTitle(String title) {
        validateStringLength(title, TITLE_MIN_LENGTH, TITLE_MAX_LENGTH, TITLE_ERROR_MESSAGE);

        if(this.title != null) {
            EventLog log = new EventLog(String.format("Title changed from %s to %s", this.title, title));
            historyLog.add(log);
        }

        this.title = title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getLogs() {
        StringBuilder builder = new StringBuilder();

        for(EventLog log : historyLog) {
            builder.append(log + System.lineSeparator());
        }

        return builder.toString();
    }

    @Override
    public void addComment(Comment comment) {
        EventLog log = new EventLog(String.format("New comment added by: %s", comment.getAuthor()));
        historyLog.add(log);

        comments.add(comment);
    }

    @Override
    public void removeComment(Comment comment) {
        EventLog log = new EventLog(String.format("Comment removed: %s", comment.getContent()));
        historyLog.add(log);

        comments.remove(comment);
    }

    private void setDescription(String description) {
        validateStringLength(description, DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH, DESCRIPTION_ERROR_MESSAGE);

        if(this.description != null) {
            EventLog log = new EventLog(String.format(DESCRIPTION_CHANGED_FROM_TO, this.description, description));
            historyLog.add(log);
        }

        this.description = description;
    }


    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getAsString() {
        return toString();
    }

    public String toString() {
        return String.format("Title: %s%n"
                + "Description: %s%n", getTitle(), getDescription());
    }
}
