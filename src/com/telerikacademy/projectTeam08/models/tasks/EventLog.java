package com.telerikacademy.projectTeam08.models.tasks;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class EventLog {
    public static final String NAME_NULL_ERROR_MESSAGE = "Name cannot be null";
    public static final String EMPTY_DESCRIPTION_ERROR_MESSAGE = "Description cannot be empty";
    private final String description;
    private final LocalDateTime timestamp;

    public EventLog(String description) {
        if(description == null)
            throw new IllegalArgumentException(NAME_NULL_ERROR_MESSAGE);
        this.description = description;
        timestamp = LocalDateTime.now();
    }

    public EventLog() {
        throw new IllegalArgumentException(EMPTY_DESCRIPTION_ERROR_MESSAGE);
    }

    public String getDescription() {
        return description;
    }

    public String viewInfo() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss");

        return String.format("[%s] %s", timestamp.format(formatter), description);
    }

    @Override
    public String toString() {
        return String.format("Log: %s", viewInfo());
    }
}
