package com.telerikacademy.projectTeam08.models.tasks;

import com.telerikacademy.projectTeam08.models.tasks.contracts.Bug;
import com.telerikacademy.projectTeam08.models.tasks.enums.PriorityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.SeverityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusBugType;
import jdk.jfr.Event;


import java.util.ArrayList;
import java.util.List;


public class BugImpl extends TaskBase implements Bug {

    public static final String NEW_STEP_ADDED = "New step added %s.";
    public static final String STEP_REMOVED = "Step removed %s.";
    public static final String THE_CURRENT_PRIORITY_ALREADY_EXISTS = "The current priority is already %s.";
    public static final String PRIORITY_CHANGED_FROM_TO = "Priority changed from %s to %s";
    public static final String THE_CURRENT_SEVERITY_ALREADY_EXISTS = "The current severity is already %s.";
    public static final String SEVERITY_CHANGED_FROM_TO = "Severity changed from %s to %s";
    public static final String BUG_STATUS_ALREADY_EXISTS = "Bug status is already %s.";
    public static final String STATUS_CHANGED_FORM_TO = "Status changed form %s to %s";
    public static final String BUG_GIVEN_TO_ASSIGNEE = "Bug given to %s";
    private PriorityType priorityType;

    private SeverityType severityType;

    private StatusBugType statusBugType;

    private String assignee;
    //Member

    private List<String> steps;


    public BugImpl(int id, String title, String description, PriorityType priorityType, SeverityType severityType,
                   StatusBugType statusType, String assignee, List<String> steps) {
        super(id, title, description);
        steps = new ArrayList<>();
        setPriorityType(priorityType);
        setSeverityType(severityType);
        setStatusBugType(statusType);
        setAssignee(assignee);
    }

    @Override
    public String getAssignee() {
        return this.assignee;
    }

    private void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    @Override
    public List<String> getSteps() {
        return new ArrayList<>(steps);
    }


    @Override
    public PriorityType getPriorityType() {
        return priorityType;
    }

    //??
    public void setPriorityType(PriorityType priorityType) {
        if (this.priorityType == priorityType) {
            throw new IllegalArgumentException(String.format(THE_CURRENT_PRIORITY_ALREADY_EXISTS, priorityType));
        }
        EventLog log = new EventLog(String.format(PRIORITY_CHANGED_FROM_TO, this.priorityType, priorityType));
        historyLog.add(log);

        this.priorityType = priorityType;
    }

    @Override
    public SeverityType getSeverityType() {
        return severityType;
    }

    //?
    public void setSeverityType(SeverityType severityType) {
        if (this.severityType == severityType) {
            throw new IllegalArgumentException(String.format(THE_CURRENT_SEVERITY_ALREADY_EXISTS, severityType));
        }
        EventLog log = new EventLog(String.format(SEVERITY_CHANGED_FROM_TO, this.severityType, severityType));
        historyLog.add(log);

        this.severityType = severityType;
    }

    @Override
    public StatusBugType getStatusBugType() {
        return statusBugType;
    }

    public void setStatusBugType(StatusBugType statusBugType) {
        if (this.statusBugType == statusBugType) {
           throw new IllegalArgumentException(String.format(BUG_STATUS_ALREADY_EXISTS, statusBugType));
        }
        EventLog log = new EventLog(String.format(STATUS_CHANGED_FORM_TO, this.statusBugType, statusBugType));
        historyLog.add(log);

        this.statusBugType = statusBugType;
    }

    @Override
    public void addBug(String titleToAdd, String assigneeToAdd) {
        System.out.println("");
    }

    @Override
    public String toString() {
        return String.format("Bug ----%n" +
                        "%s" + "Priority: %s%n" + "Severity: %s%n" +
                        "Status: %s%n", super.toString(),
                getPriorityType(), getSeverityType(), getStatusBugType());
    }
}
