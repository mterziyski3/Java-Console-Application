package com.telerikacademy.projectTeam08.models.contracts;

import com.telerikacademy.projectTeam08.models.tasks.EventLog;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;

import java.util.List;

public interface Board {
    void addTask(Task task);
    void removeTask(Task task);

    String getName();
    List<EventLog> showActivity();
}
