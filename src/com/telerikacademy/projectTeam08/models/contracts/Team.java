package com.telerikacademy.projectTeam08.models.contracts;

import com.telerikacademy.projectTeam08.models.tasks.EventLog;

import java.util.List;

public interface Team {

    String getName();

    List<Board> getBoards();

    List<Member> getMembers();

    List<EventLog> showActivity();

}
