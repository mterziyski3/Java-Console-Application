package com.telerikacademy.projectTeam08.models.contracts;

import com.telerikacademy.projectTeam08.models.tasks.EventLog;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Bug;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Comment;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;

import java.util.List;

public interface Member {

    String getName();

    List<Task> getTasks();

    void addComment(Comment commentToAdd, Task taskToAdd);

    void removeComment(Comment commentToRemove, Task taskToRemoveComment);

    List<EventLog> showActivity();

     List<Bug> getBugs();
}
