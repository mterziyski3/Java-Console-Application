package com.telerikacademy.projectTeam08.models.contracts;

public interface Printable {

    String getAsString();

}
