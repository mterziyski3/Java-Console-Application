package com.telerikacademy.projectTeam08.models;

import com.telerikacademy.projectTeam08.models.contracts.Board;
import com.telerikacademy.projectTeam08.models.contracts.Member;
import com.telerikacademy.projectTeam08.models.contracts.Team;
import com.telerikacademy.projectTeam08.models.tasks.EventLog;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {

    public static final int TEAM_NAME_MIN_LENGTH = 5;
    public static final int TEAM_NAME_MAX_LENGTH = 15;
    private static final String TEAM_NAME_LENGTH_ERR = "The team name should be between 5 and 15 symbols";

    public static final String NAME_CHANGED_FROM_TO = "Name changed from %s to %s.";


    private String name;
    private final List <Member> members;
    private final List <Board> boards;
    private List <EventLog> activityHistory;

    public TeamImpl(String name) {
        setName(name);
        this.members = new ArrayList<>();
        this.boards = new ArrayList<>();

        activityHistory = new ArrayList<>();
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        ValidationHelpers.validateIntRange(name.length(),
                TEAM_NAME_MIN_LENGTH, TEAM_NAME_MAX_LENGTH, TEAM_NAME_LENGTH_ERR);

        if(this.name != null) {
            EventLog log = new EventLog(String.format(NAME_CHANGED_FROM_TO, this.name, name));
            activityHistory.add(log);
        }
        this.name = name;
    }

    public List<Member> getMembers() {
        return this.members;
    }

    @Override
    public List<EventLog> showActivity() {
        return activityHistory;
    }

    public List<Board> getBoards() {
        return this.boards;
    }

    @Override
    public String toString() {
        return String.format("Team: %s", name);
    }
}
