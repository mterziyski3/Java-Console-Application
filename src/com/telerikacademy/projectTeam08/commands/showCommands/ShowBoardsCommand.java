package com.telerikacademy.projectTeam08.commands.showCommands;

import com.telerikacademy.projectTeam08.commands.contracts.Command;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.contracts.Board;
import com.telerikacademy.projectTeam08.models.contracts.Member;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ShowBoardsCommand implements Command {

    // ShowBoards

    private TaskBoardRepository taskBoardRepository;

    public ShowBoardsCommand(TaskBoardRepository taskBoardRepository) {
        this.taskBoardRepository = taskBoardRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        return showBoards();
    }

    private String showBoards() {
        List<Board> boards = taskBoardRepository.showBoards();

        StringBuilder stringBuilder = new StringBuilder();
        int counter = 1;

        stringBuilder.append("--BOARDS--");
        stringBuilder.append(System.lineSeparator());

        for (Board board : boards) {
            stringBuilder.append(String.format("%d. %s", counter, board.toString()));
            stringBuilder.append(System.lineSeparator());
            counter++;
        }
        return stringBuilder.toString().trim();
    }
}
