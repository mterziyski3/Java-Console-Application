package com.telerikacademy.projectTeam08.commands.showCommands;

import com.telerikacademy.projectTeam08.commands.contracts.Command;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.contracts.Board;
import com.telerikacademy.projectTeam08.models.tasks.EventLog;

import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

public class ShowBoardActivityCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private TaskBoardRepository taskBoardRepository;

    private String name;


    public ShowBoardActivityCommand(TaskBoardRepository taskBoardRepository) {
        this.taskBoardRepository = taskBoardRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return showBoardActivity(name);
    }

    private String showBoardActivity(String name) {
        List<EventLog> boardActivityLogs = taskBoardRepository.showBoardActivity(name);

        StringBuilder stringBuilder = new StringBuilder();
        int counter = 1;

        stringBuilder.append("--ACTIVITY LOGS--");
        stringBuilder.append(System.lineSeparator());

        for (EventLog log : boardActivityLogs) {
            stringBuilder.append(String.format("%d. %s", counter, log.toString()));
            stringBuilder.append(System.lineSeparator());
            counter++;
        }
        return stringBuilder.toString().trim();
    }

    private void parseParameters(List<String> parameters) {
        name = parameters.get(0);
    }
}
