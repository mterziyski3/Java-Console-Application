package com.telerikacademy.projectTeam08.commands.showCommands;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.contracts.Team;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

public class ShowTeamsCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    public ShowTeamsCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        return showTeams();
    }

    private String showTeams() {

        StringBuilder stringBuilder = new StringBuilder();
        int counter = 1;

        stringBuilder.append("--TEAMS--");
        stringBuilder.append(System.lineSeparator());

        for (Team team : getTaskBoardRepository().getTeams()) {
            stringBuilder.append(String.format("%d. %s", counter, team.toString()));
            stringBuilder.append(System.lineSeparator());
            counter++;
        }

        return stringBuilder.toString().trim();
    }

 }
