package com.telerikacademy.projectTeam08.commands.showCommands;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.contracts.Member;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

public class ShowMembersCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    public ShowMembersCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        return showMembersTasks();
    }

    private String showMembersTasks() {
        StringBuilder stringBuilder = new StringBuilder();
        int counter = 1;

        stringBuilder.append("--MEMBERS--");
        stringBuilder.append(System.lineSeparator());

        for (Member member : getTaskBoardRepository().getMembers()) {
            stringBuilder.append(String.format("%d. %s", counter, member.toString()));
            stringBuilder.append(System.lineSeparator());
            counter++;
        }

        return stringBuilder.toString().trim();
    }
}
