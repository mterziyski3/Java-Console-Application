package com.telerikacademy.projectTeam08.commands.showCommands;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.contracts.Team;
import com.telerikacademy.projectTeam08.models.tasks.EventLog;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

public class ShowTeamActivityCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private String name;

    public ShowTeamActivityCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        name = parameters.get(0);

        return ShowTeamActivity(name);
    }
    //?
    private String ShowTeamActivity(String name) {
        List<EventLog> teamActivityLog = getTaskBoardRepository().showTeamActivity(name);

        StringBuilder stringBuilder = new StringBuilder();
        for (EventLog EventLog : getTaskBoardRepository().showTeamActivity(name)
        ) {
            stringBuilder.append(String.format("%s", EventLog.toString()));
            stringBuilder.append(System.lineSeparator());
        }
        return stringBuilder.toString().trim();
    }

}
