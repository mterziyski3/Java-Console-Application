package com.telerikacademy.projectTeam08.commands.showCommands;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.EventLog;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

public class ShowMemberActivityCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private String name;

    public ShowMemberActivityCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        name = parameters.get(0);
        return showMemberActivity(name);
    }

    private String showMemberActivity(String name) {

        List<EventLog> memberActivityLog = getTaskBoardRepository().showMemberActivity(name);

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("--MEMBER'S ACTIVITY--");
        stringBuilder.append(System.lineSeparator());
        for (EventLog eventLog : getTaskBoardRepository().showMemberActivity(name)) {
            stringBuilder.append(String.format("%s", eventLog.toString()));
            stringBuilder.append(System.lineSeparator());
        }
        return stringBuilder.toString().trim();
    }
}
