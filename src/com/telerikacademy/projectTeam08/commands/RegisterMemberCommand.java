package com.telerikacademy.projectTeam08.commands;

import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.contracts.Member;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

public class RegisterMemberCommand extends BaseCommand {

    private final static String MEMBER_REGISTERED = "Member %s registered successfully!";
    private final static String MEMBER_ALREADY_EXIST = "Member %s already exist. Choose a different name!";
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public RegisterMemberCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String name = parameters.get(0);

        return registerMember(name);
    }

    private String registerMember(String name) {

        if (getTaskBoardRepository().getMembers().stream().anyMatch(u -> u.getName().equalsIgnoreCase(name))) {
            throw new IllegalArgumentException(String.format(MEMBER_ALREADY_EXIST, name));
        }

        Member member = getTaskBoardRepository().createMember(name);
        getTaskBoardRepository().addMember(member);

        return String.format(MEMBER_REGISTERED, name);

    }
}
