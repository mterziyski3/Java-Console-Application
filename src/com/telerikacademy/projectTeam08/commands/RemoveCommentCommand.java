package com.telerikacademy.projectTeam08.commands;

import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.contracts.Member;
import com.telerikacademy.projectTeam08.models.tasks.contracts.*;
import com.telerikacademy.projectTeam08.utils.ParsingHelpers;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;


import java.util.List;

public class RemoveCommentCommand extends BaseCommand {

    private final static String COMMENT_INDEX_OUT_OF_BOUNDS = "There is no comment on this index.";
    public final static String INVALID_COMMENT_INDEX = "Invalid comment index. Expected a number.";
    public final static String COMMENT_REMOVED_SUCCESSFULLY = "%s removed comment successfully!";
    private final static String INVALID_TASK_INDEX = "Invalid vehicle index. Expected a number.";
    private final static String TASK_INDEX_OUT_OF_BOUNDS = "There is no vehicle on this index.";

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    public RemoveCommentCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        int taskIndex = ParsingHelpers.tryParseInt(parameters.get(0), INVALID_TASK_INDEX);
        int commentIndex = ParsingHelpers.tryParseInt(parameters.get(1), INVALID_COMMENT_INDEX);
        String name = parameters.get(2);
        return removeComment(taskIndex, commentIndex, name);
    }

    private String removeComment(int taskIndex, int commentIndex, String name) {
        Member member = getTaskBoardRepository().findMemberByName(name);

        ValidationHelpers.validateIntRange(taskIndex, 0, member.getTasks().size(), TASK_INDEX_OUT_OF_BOUNDS);
        ValidationHelpers.validateIntRange(commentIndex, 0, member.getTasks().get(taskIndex).getComments().size(),
                COMMENT_INDEX_OUT_OF_BOUNDS);

        Task task = member.getTasks().get(taskIndex);
        Comment comment = member.getTasks().get(taskIndex).getComments().get(commentIndex);

        getTaskBoardRepository().getAddedMember().removeComment(comment, task);

        return String.format(COMMENT_REMOVED_SUCCESSFULLY, getTaskBoardRepository().getAddedMember().getName());
    }
}
