package com.telerikacademy.projectTeam08.commands.listing;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.utils.ListingHelpers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ListStoriesCommand extends BaseCommand {

    private static final String NO_REGISTERED_STORIES = "There are no registered stories.";

    private List<Task> stories;

    public ListStoriesCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
        stories = taskBoardRepository.getStories();
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        if (stories.isEmpty()) {
            throw new IllegalArgumentException(NO_REGISTERED_STORIES);
        }
        StringBuilder output = new StringBuilder();

        stories = stories
                .stream()
                .sorted(Comparator.comparing(Task::getTitle).thenComparing(Task::getPriorityType))
                .collect(Collectors.toList());


        for (Task story : stories) {
            output.append(ListingHelpers.storiesToString(List.of(story)));

            output.append(String.format("%nAssignee: %s%n", story.getAssignee()));
        }

        return output.toString();
    }
}
