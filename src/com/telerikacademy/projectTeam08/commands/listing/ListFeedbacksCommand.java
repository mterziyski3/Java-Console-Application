package com.telerikacademy.projectTeam08.commands.listing;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.utils.ListingHelpers;

import java.util.List;

public class ListFeedbacksCommand extends BaseCommand {

    private static final String NO_REGISTERED_FEEDBACKS = "There are no registered feedbacks.";

    private final List<Task> feedbacks;

    public ListFeedbacksCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
        feedbacks = taskBoardRepository.getFeedbacks();
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        if(feedbacks.isEmpty()) {
            throw new IllegalArgumentException(NO_REGISTERED_FEEDBACKS);
        }
        return ListingHelpers.feedbacksToString(feedbacks);
    }
}
