package com.telerikacademy.projectTeam08.commands.listing;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Assignable;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Bug;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.utils.ListingHelpers;

import java.util.List;

public class ListBugsCommand extends BaseCommand {

    private static final String NO_REGISTERED_BUGS = "There are no registered bugs.";
    private final List<Task> bugs;


    public ListBugsCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
        bugs = taskBoardRepository.getBugs();
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        if (bugs.isEmpty()) {
            throw new IllegalArgumentException(NO_REGISTERED_BUGS);
        }

        StringBuilder output = new StringBuilder();

        for(Task bug : bugs) {
            output.append(ListingHelpers.bugsToString(List.of(bug)));

            output.append(String.format("%nAssignee: %s", bug.getAssignee()));
        }

        return output.toString();
    }
}
