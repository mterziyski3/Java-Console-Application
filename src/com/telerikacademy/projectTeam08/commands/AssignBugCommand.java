package com.telerikacademy.projectTeam08.commands;

import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;


import com.telerikacademy.projectTeam08.models.contracts.Member;
import com.telerikacademy.projectTeam08.models.contracts.Team;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Bug;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

public class AssignBugCommand extends BaseCommand {

    public final static String BUG_DOES_NOT_EXIST = "The bug does not exist!";

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String IS_ASSIGNED_WITH_BUG_WITH_ID_S = "The member is assigned with bug with id: %s";


    public AssignBugCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {

        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int id = Integer.parseInt(parameters.get(0));
        String assignee =parameters.get(1);

        return assignBug(id, assignee);
    }

    private String assignBug(int id, String assignee) {

//        Team team = getTaskBoardRepository().getTeams();

        Member member = getTaskBoardRepository().findMemberByName(assignee);

        Bug bug = member.getBugs().get(id);

        return String.format(IS_ASSIGNED_WITH_BUG_WITH_ID_S, getTaskBoardRepository().getBugs().get(id));
    }
}
