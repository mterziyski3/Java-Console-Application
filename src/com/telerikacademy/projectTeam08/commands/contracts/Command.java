package com.telerikacademy.projectTeam08.commands.contracts;

import java.util.List;

public interface Command {
    String execute(List<String> parameters);
}
