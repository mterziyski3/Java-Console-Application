package com.telerikacademy.projectTeam08.commands;


import com.telerikacademy.projectTeam08.commands.contracts.Command;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;

import java.util.List;

public abstract class BaseCommand implements Command {

    private final TaskBoardRepository taskBoardRepository;

    protected BaseCommand(TaskBoardRepository taskBoardRepository) {
        this.taskBoardRepository = taskBoardRepository;
    }

    protected TaskBoardRepository getTaskBoardRepository() {
        return taskBoardRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        return executeCommand(parameters);
    }

    protected abstract String executeCommand(List<String> parameters);
}
