package com.telerikacademy.projectTeam08.commands;

import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.contracts.Member;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Comment;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.utils.ParsingHelpers;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

public class AddCommentCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;
    public static final String INVALID_INPUT_MESSAGE = "Invalid input. Expected a number.";
    public final static String COMMENT_ADDED_SUCCESSFULLY = "%s added comment successfully!";
    public final static String TASK_DOES_NOT_EXIST = "The task does not exist!";

    public AddCommentCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String content = parameters.get(0);
        String author = parameters.get(1);
        int taskIndex = ParsingHelpers.tryParseInt(parameters.get(2), INVALID_INPUT_MESSAGE) - 1;
        return addComment(content, taskIndex, author);
    }

    private String addComment(String content, int taskIndex, String author) {
        Member member = getTaskBoardRepository().findMemberByName(author);

        ValidationHelpers.validateIntRange(taskIndex, 0, member.getTasks().size(), TASK_DOES_NOT_EXIST);

        Task task = member.getTasks().get(taskIndex);

        Comment comment = getTaskBoardRepository().createComment(content, getTaskBoardRepository().getAddedMember().getName());

        getTaskBoardRepository().getAddedMember().addComment(comment, task);

        return String.format(COMMENT_ADDED_SUCCESSFULLY, getTaskBoardRepository().getAddedMember().getName());


    }
}
