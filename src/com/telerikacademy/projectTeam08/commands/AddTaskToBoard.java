package com.telerikacademy.projectTeam08.commands;

import com.telerikacademy.projectTeam08.commands.contracts.Command;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.utils.ParsingHelpers;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

public class AddTaskToBoard implements Command {
    public static final String INVALID_NUMBER_ERROR_MESSAGE = "Invalid number.";
    public static String TASKADDEDTOBOARD = "Task %s added to Board %s";
    // AddTaskToBord Task boardName

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private TaskBoardRepository taskBoardRepository;

    private String boardName;
    private int taskId;


    public AddTaskToBoard(TaskBoardRepository taskBoardRepository) {
        this.taskBoardRepository = taskBoardRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return addTaskToBoard(taskId, boardName);
    }

    private String addTaskToBoard(int taskId, String boardName) {
        Task task = taskBoardRepository.addTaskToBoard(taskId, boardName);

        return String.format(TASKADDEDTOBOARD, task.getTitle(), boardName);
    }

    private void parseParameters(List<String> parameters) {
        taskId = ParsingHelpers.tryParseInt(parameters.get(0), INVALID_NUMBER_ERROR_MESSAGE);
        boardName = parameters.get(1);
    }
}
