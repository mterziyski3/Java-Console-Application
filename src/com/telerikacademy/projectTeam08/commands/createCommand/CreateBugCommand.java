package com.telerikacademy.projectTeam08.commands.createCommand;

import com.telerikacademy.projectTeam08.commands.contracts.Command;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Bug;
import com.telerikacademy.projectTeam08.models.tasks.enums.PriorityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.SeverityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusBugType;
import com.telerikacademy.projectTeam08.utils.ParsingHelpers;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;


import java.util.List;

public class CreateBugCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 7;
    private static final String BUG_CREATED_MESSAGE = "Bug with ID %d was created.";
    //CommandsConstants?

    private TaskBoardRepository taskBoardRepository;

    private String title;
    private String description;
    private PriorityType priorityType;
    private SeverityType severityType;
    private StatusBugType statusType;
    private String assignee;

    private List <String> steps;


    public CreateBugCommand(TaskBoardRepository taskBoardRepository) {
        this.taskBoardRepository = taskBoardRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return addBug(title, description, priorityType, severityType, statusType, assignee, steps);
    }

    private String addBug(String title, String description, PriorityType priorityType,
                          SeverityType severityType, StatusBugType statusType, String assignee,List <String> steps) {
        Bug createdBug = taskBoardRepository.createBug(title, description, priorityType, severityType, statusType, assignee, steps);

        return String.format(BUG_CREATED_MESSAGE, createdBug.getId());

    }

    private void parseParameters(List<String> parameters) {
        title = parameters.get(0);
        description = parameters.get(1);
        priorityType = ParsingHelpers.tryParseEnum(parameters.get(2), PriorityType.class);
        severityType = ParsingHelpers.tryParseEnum(parameters.get(3), SeverityType.class);
        statusType = ParsingHelpers.tryParseEnum(parameters.get(4), StatusBugType.class);
        assignee = parameters.get(5);
        String [] steps = parameters.get(6).split(" ");
    }
}
