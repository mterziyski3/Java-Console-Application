package com.telerikacademy.projectTeam08.commands.createCommand;

import com.telerikacademy.projectTeam08.commands.contracts.Command;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.contracts.Board;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

public class CreateBoardCommand implements Command {

    // CreateBoard name

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String BOARD_ADDED_MESSAGE = "Board added - %s";

    private final TaskBoardRepository taskBoardRepository;

    private String name;


    public CreateBoardCommand(TaskBoardRepository taskBoardRepository) {
        this.taskBoardRepository = taskBoardRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return addBoard(name);
    }

    private String addBoard(String name) {
        Board board = taskBoardRepository.createBoard(name);

        return String.format(BOARD_ADDED_MESSAGE, name);
    }

    private void parseParameters(List<String> parameters) {
        name = parameters.get(0);
    }
}
