package com.telerikacademy.projectTeam08.commands.createCommand;


import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Feedback;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusFeedbackType;
import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.*;
import static com.telerikacademy.projectTeam08.utils.ValidationHelpers.*;

import java.util.List;

public class CreateFeedbackCommand extends BaseCommand {

    public static final int EXCEPTED_NUMBER_OF_ARGUMENTS = 4;
    private static final String INVALID_NUMBER_FIELD_MESSAGE = "Invalid value for %s. Should be a number.";
    private static final String FEEDBACK_CREATED_MESSAGE = "Feedback with ID %d was created.";

    private String title;
    private String description;
    private int rating;

    private StatusFeedbackType statusFeedbackType;


    public CreateFeedbackCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        validateArgumentsCount(parameters, EXCEPTED_NUMBER_OF_ARGUMENTS);

        title = parameters.get(0);
        description = parameters.get(1);
        rating = tryParseInt(parameters.get(2), INVALID_NUMBER_FIELD_MESSAGE);
        StatusFeedbackType statusFeedbackType = tryParseEnum(parameters.get(3), StatusFeedbackType.class);

        Feedback createdFeedback = getTaskBoardRepository().createFeedback(title, description, rating,
                             statusFeedbackType);


        return String.format(FEEDBACK_CREATED_MESSAGE, createdFeedback.getId());


    }
}
