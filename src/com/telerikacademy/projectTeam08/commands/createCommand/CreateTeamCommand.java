package com.telerikacademy.projectTeam08.commands.createCommand;


import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.contracts.Team;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

public class CreateTeamCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String TEAM_CREATED = "Team with name %s was created!";

    private String name;

    public CreateTeamCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        name = parameters.get(0);

        Team createdTeam = getTaskBoardRepository().createTeam(name);
        getTaskBoardRepository().addTeam(createdTeam);

        return String.format(TEAM_CREATED, name);
    }
}
