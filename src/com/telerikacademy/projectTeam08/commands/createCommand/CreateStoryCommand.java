package com.telerikacademy.projectTeam08.commands.createCommand;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Story;
import com.telerikacademy.projectTeam08.models.tasks.enums.PriorityType;
import com.telerikacademy.projectTeam08.models.tasks.enums.SizeType;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusStoryType;

import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.*;
import static com.telerikacademy.projectTeam08.utils.ValidationHelpers.*;

import java.util.List;

public class CreateStoryCommand extends BaseCommand {

    public static final int EXCEPTED_NUMBER_OF_ARGUMENTS = 6;
    private static final String STORY_CREATED_MESSAGE = "Story with ID %d was created.";

    private String title;
    private String description;
    private PriorityType priorityType;
    private SizeType sizeType;
    private StatusStoryType statusStoryType;
    private String assignee;

    public CreateStoryCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
    }



    @Override
    protected String executeCommand(List<String> parameters) {
        validateArgumentsCount(parameters, EXCEPTED_NUMBER_OF_ARGUMENTS);

        title = parameters.get(0);
        description = parameters.get(1);
        PriorityType priorityType = tryParseEnum(parameters.get(2), PriorityType.class);
        SizeType sizeType = tryParseEnum(parameters.get(3), SizeType.class);
        StatusStoryType statusStoryType = tryParseEnum(parameters.get(4), StatusStoryType.class);
        assignee = parameters.get (5);

        Story createdStory = getTaskBoardRepository().createStory(title, description, priorityType,
                sizeType, statusStoryType, assignee);

        return String.format(STORY_CREATED_MESSAGE, createdStory.getId());
    }
}
