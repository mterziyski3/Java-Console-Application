package com.telerikacademy.projectTeam08.commands.modification;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Story;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.models.tasks.enums.SizeType;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusStoryType;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.tryParseEnum;
import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.tryParseInt;

public class ChangeStoryStatusCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String STORY_STATUS_CHANGED = "Story status changed from %s to %s.";
    public static final String STATUS_HAS_BEEN_ALREADY_SET_TO_THIS_STATUS = "Status has been already set to this status";
    public static final String INVALID_STORY_ID = "Invalid Story Id";

    private int id;
    private StatusStoryType statusStoryType;

    private final List<Task> tasks;

    public ChangeStoryStatusCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
        tasks = taskBoardRepository.getStories();
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        id = tryParseInt(parameters.get(0), INVALID_STORY_ID);
        statusStoryType = tryParseEnum(parameters.get(1), StatusStoryType.class);

        Story story = (Story) getTaskBoardRepository().findTaskById(id);

        StatusStoryType statusStoryType = story.getStatusStoryType();

        if (this.statusStoryType == statusStoryType) {
            throw new IllegalArgumentException(STATUS_HAS_BEEN_ALREADY_SET_TO_THIS_STATUS);

        }

        story.setStatusStoryType(this.statusStoryType);

        return String.format(STORY_STATUS_CHANGED, statusStoryType, this.statusStoryType);

    }
}
