package com.telerikacademy.projectTeam08.commands.modification;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Bug;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusBugType;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.tryParseEnum;
import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.tryParseInt;

public class ChangeBugStatusCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String BUG_STATUS_CHANGED = "Bug status changed from %s to %s.";
    public static final String STATUS_HAS_BEEN_ALREADY_SET_TO_THIS_STATE = "Status has been already set to this state";
    public static final String INVALID_BUG_ID = "Invalid Bug Id";

    private int id;
    private StatusBugType statusBugType;

    private final List<Task> tasks;

    public ChangeBugStatusCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
        tasks = taskBoardRepository.getBugs();
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        id = tryParseInt(parameters.get(0), INVALID_BUG_ID);
        statusBugType = tryParseEnum(parameters.get(1), StatusBugType.class);

        Bug bug = (Bug) getTaskBoardRepository().findTaskById(id);

        StatusBugType statusBugType = bug.getStatusBugType();

        if (this.statusBugType == statusBugType) {
            throw new IllegalArgumentException(STATUS_HAS_BEEN_ALREADY_SET_TO_THIS_STATE);

        }

        bug.setStatusBugType(this.statusBugType);

        return String.format(BUG_STATUS_CHANGED, statusBugType, this.statusBugType);

    }
}
