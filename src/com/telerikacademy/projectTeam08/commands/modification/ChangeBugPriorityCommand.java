package com.telerikacademy.projectTeam08.commands.modification;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Bug;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.models.tasks.enums.PriorityType;
import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.*;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

public class ChangeBugPriorityCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String BUG_PRIORITY_CHANGED = "Bug priority changed from %s to %s.";
    public static final String PRIORITY_HAS_BEEN_ALREADY_SET_TO_THIS_PRIORITY = "Priority has been already set to this state";
    public static final String INVALID_BUG_ID = "Invalid Bug Id";

    private int id;
    private PriorityType priorityType;

    private final List<Task> tasks;

    public ChangeBugPriorityCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
        tasks = taskBoardRepository.getBugs();
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        id = tryParseInt(parameters.get(0), INVALID_BUG_ID);
        priorityType = tryParseEnum(parameters.get(1), PriorityType.class);

        Bug bug = (Bug) getTaskBoardRepository().findTaskById(id);

        PriorityType priorityType = bug.getPriorityType();

        if (this.priorityType == priorityType) {
            throw new IllegalArgumentException(PRIORITY_HAS_BEEN_ALREADY_SET_TO_THIS_PRIORITY);

        }

        bug.setPriorityType(this.priorityType);

        return String.format(BUG_PRIORITY_CHANGED, priorityType, this.priorityType);

    }
}
