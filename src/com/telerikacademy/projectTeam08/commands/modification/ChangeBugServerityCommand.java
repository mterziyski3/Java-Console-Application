package com.telerikacademy.projectTeam08.commands.modification;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Bug;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.models.tasks.enums.SeverityType;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.tryParseEnum;
import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.tryParseInt;

public class ChangeBugServerityCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String BUG_SEVERITY_CHANGED = "Bug severity changed from %s to %s.";
    public static final String SEVERITY_HAS_BEEN_ALREADY_SET_TO_THIS_SEVERITY = "Severity has been already set to this state";
    public static final String INVALID_BUG_ID = "Invalid Bug Id";

    private int id;
    private SeverityType severityType;
    private final List<Task> tasks;

    public ChangeBugServerityCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
        tasks = taskBoardRepository.getBugs();
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        id = tryParseInt(parameters.get(0), INVALID_BUG_ID);
        severityType = tryParseEnum(parameters.get(1), SeverityType.class);

        Bug bug = (Bug) getTaskBoardRepository().findTaskById(id);

        SeverityType severityType = bug.getSeverityType();

        if (this.severityType == severityType) {
            throw new IllegalArgumentException(SEVERITY_HAS_BEEN_ALREADY_SET_TO_THIS_SEVERITY);

        }

        bug.setSeverityType(this.severityType);

        return String.format(BUG_SEVERITY_CHANGED, severityType, this.severityType);

    }
}
