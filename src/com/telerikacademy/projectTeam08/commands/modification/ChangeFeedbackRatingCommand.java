package com.telerikacademy.projectTeam08.commands.modification;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Feedback;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.tryParseInt;

public class ChangeFeedbackRatingCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String FEEDBACK_RATING_CHANGED = "Feedback rating changed from %s to %s.";
    public static final String RATING_HAS_BEEN_ALREADY_SET_TO_THIS_RATING = "Rating has been already set to this rating";
    public static final String INVALID_STORY_ID = "Invalid Story Id";
    public static final String INVALID_NUMBER = "Should be a number";

    private int id;
    private int rating;

    private final List<Task> feedbacks;

    public ChangeFeedbackRatingCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
        feedbacks = taskBoardRepository.getStories();
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        id = tryParseInt(parameters.get(0), INVALID_STORY_ID);
        rating = tryParseInt(parameters.get(1), INVALID_NUMBER);

        Feedback feedback = (Feedback) getTaskBoardRepository().findTaskById(id);

        int rating = feedback.getRating();

        if (this.rating == rating) {
            throw new IllegalArgumentException(RATING_HAS_BEEN_ALREADY_SET_TO_THIS_RATING);

        }

        feedback.setRating(this.rating);

        return String.format(FEEDBACK_RATING_CHANGED, rating, this.rating);
    }
}
