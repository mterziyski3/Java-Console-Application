package com.telerikacademy.projectTeam08.commands.modification;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Story;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.models.tasks.enums.SizeType;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.tryParseEnum;
import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.tryParseInt;

public class ChangeStorySizeCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String STORY_SIZE_CHANGED = "Story size changed from %s to %s.";
    public static final String SIZE_HAS_BEEN_ALREADY_SET_TO_THIS_SIZE = "Size has been already set to this size";
    public static final String INVALID_STORY_ID = "Invalid Story Id";

    private int id;
    private SizeType sizeType;

    private final List<Task> tasks;

    public ChangeStorySizeCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
        tasks = taskBoardRepository.getStories();
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        id = tryParseInt(parameters.get(0), INVALID_STORY_ID);
        sizeType = tryParseEnum(parameters.get(1), SizeType.class);

        Story story = (Story) getTaskBoardRepository().findTaskById(id);

        SizeType sizeType = story.getSizeType();

        if (this.sizeType == sizeType) {
            throw new IllegalArgumentException(SIZE_HAS_BEEN_ALREADY_SET_TO_THIS_SIZE);

        }

        story.setSizeType(this.sizeType);

        return String.format(STORY_SIZE_CHANGED, sizeType, this.sizeType);

    }
}
