package com.telerikacademy.projectTeam08.commands.modification;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Feedback;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.models.tasks.enums.StatusFeedbackType;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.tryParseEnum;
import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.tryParseInt;

public class ChangeFeedbackStatusCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String FEEDBACK_STATUS_CHANGED = "Feedback status changed from %s to %s.";
    public static final String STATUS_HAS_BEEN_ALREADY_SET_TO_THIS_STATUS = "Status has been already set to this status";
    public static final String INVALID_STORY_ID = "Invalid Feedback Id";

    private int id;
    private StatusFeedbackType statusFeedbackType;

    private final List<Task> tasks;

    public ChangeFeedbackStatusCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
        tasks = taskBoardRepository.getStories();
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        id = tryParseInt(parameters.get(0), INVALID_STORY_ID);
        statusFeedbackType = tryParseEnum(parameters.get(1), StatusFeedbackType.class);

        Feedback feedback = (Feedback) getTaskBoardRepository().findTaskById(id);

        StatusFeedbackType statusFeedbackType = feedback.getStatusFeedbackType();

        if (this.statusFeedbackType == statusFeedbackType) {
            throw new IllegalArgumentException(STATUS_HAS_BEEN_ALREADY_SET_TO_THIS_STATUS);

        }

        feedback.setStatusFeedbackType(this.statusFeedbackType);

        return String.format(FEEDBACK_STATUS_CHANGED, statusFeedbackType, this.statusFeedbackType);

    }
}
