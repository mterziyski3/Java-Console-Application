package com.telerikacademy.projectTeam08.commands.modification;

import com.telerikacademy.projectTeam08.commands.BaseCommand;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Story;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;
import com.telerikacademy.projectTeam08.models.tasks.enums.PriorityType;
import com.telerikacademy.projectTeam08.utils.ValidationHelpers;

import java.util.List;

import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.tryParseEnum;
import static com.telerikacademy.projectTeam08.utils.ParsingHelpers.tryParseInt;

public class ChangeStoryPriorityCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String STORY_PRIORITY_CHANGED = "Story priority changed from %s to %s.";
    public static final String PRIORITY_HAS_BEEN_ALREADY_SET_TO_THIS_PRIORITY = "Priority has been already set to this priority";
    public static final String INVALID_STORY_ID = "Invalid Story Id";

    private int id;
    private PriorityType priorityType;

    private final List<Task> tasks;

    public ChangeStoryPriorityCommand(TaskBoardRepository taskBoardRepository) {
        super(taskBoardRepository);
        tasks = taskBoardRepository.getStories();
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        id = tryParseInt(parameters.get(0), INVALID_STORY_ID);
        priorityType = tryParseEnum(parameters.get(1), PriorityType.class);

        Story story = (Story) getTaskBoardRepository().findTaskById(id);

        PriorityType priorityType = story.getPriorityType();

        if (this.priorityType == priorityType) {
            throw new IllegalArgumentException(PRIORITY_HAS_BEEN_ALREADY_SET_TO_THIS_PRIORITY);

        }

        story.setPriorityType(this.priorityType);

        return String.format(STORY_PRIORITY_CHANGED, priorityType, this.priorityType);

    }
}
