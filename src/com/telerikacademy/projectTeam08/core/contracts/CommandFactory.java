package com.telerikacademy.projectTeam08.core.contracts;

import com.telerikacademy.projectTeam08.commands.contracts.Command;

public interface CommandFactory {
    Command createCommandFromCommandName(String commandTypeAsString, TaskBoardRepository taskBoardRepository);
}
