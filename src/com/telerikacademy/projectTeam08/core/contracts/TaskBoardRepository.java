package com.telerikacademy.projectTeam08.core.contracts;

import com.telerikacademy.projectTeam08.models.contracts.Board;
import com.telerikacademy.projectTeam08.models.contracts.Member;
import com.telerikacademy.projectTeam08.models.contracts.Team;
import com.telerikacademy.projectTeam08.models.tasks.EventLog;
import com.telerikacademy.projectTeam08.models.tasks.contracts.*;
import com.telerikacademy.projectTeam08.models.tasks.enums.*;


import java.util.List;

public interface TaskBoardRepository {

    Bug createBug(String title, String description, PriorityType priorityType, SeverityType severityType,
                  StatusBugType statusType, String assignee, List<String> steps);

    Story createStory(String title, String description, PriorityType priorityType, SizeType sizeType,
                      StatusStoryType statusStoryType, String assignee);

    Feedback createFeedback(String title, String description, int rating, StatusFeedbackType statusFeedbackType);

    Comment createComment(String content, String author);

    List<Member> getMembers();

    void addMember(Member memberToAdd);

    Member findMemberByName (String name);

    Member getAddedMember();

    List<Team> getTeams();

    Team createTeam(String name);

    void addTeam(Team teamToAdd);

    List <EventLog> showTeamActivity(String name);

    List<EventLog> showMemberActivity(String name);

    Member createMember(String name);

    Board createBoard(String name);

    List<Task> getBugs();

    List<Board> getBoards();

    List<Task> getFeedbacks();

    List<Task> getStories();

    List<Board> showBoards();

    List<EventLog> showBoardActivity(String name);

    Task addTaskToBoard(int taskId, String boardName);

    Task findTaskById (int taskId);


    List<Task> getTasks();
}
