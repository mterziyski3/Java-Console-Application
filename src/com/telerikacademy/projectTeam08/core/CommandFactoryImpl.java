package com.telerikacademy.projectTeam08.core;

import com.telerikacademy.projectTeam08.commands.*;
import com.telerikacademy.projectTeam08.commands.contracts.Command;
import com.telerikacademy.projectTeam08.commands.createCommand.*;
import com.telerikacademy.projectTeam08.commands.enums.CommandType;
import com.telerikacademy.projectTeam08.commands.listing.ListBugsCommand;
import com.telerikacademy.projectTeam08.commands.listing.ListFeedbacksCommand;
import com.telerikacademy.projectTeam08.commands.listing.ListStoriesCommand;
import com.telerikacademy.projectTeam08.commands.listing.ListTasksCommand;
import com.telerikacademy.projectTeam08.commands.modification.*;
import com.telerikacademy.projectTeam08.commands.showCommands.*;
import com.telerikacademy.projectTeam08.core.contracts.CommandFactory;
import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.utils.ParsingHelpers;

public class CommandFactoryImpl implements CommandFactory {


    @Override
    public Command createCommandFromCommandName(String commandTypeAsString, TaskBoardRepository taskBoardRepository) {
        CommandType commandType = ParsingHelpers.tryParseEnum(commandTypeAsString, CommandType.class);
        switch (commandType) {
            case ADDCOMMENT:
                return new AddCommentCommand(taskBoardRepository);
            case REMOVECOMMENT:
                return new RemoveCommentCommand(taskBoardRepository);
            case CREATEBUG:
                return new CreateBugCommand(taskBoardRepository);
            case CREATESTORY:
                return new CreateStoryCommand(taskBoardRepository);
            case CREATEFEEDBACK:
                return new CreateFeedbackCommand(taskBoardRepository);
            case SHOWTEAMS:
                return new ShowTeamsCommand(taskBoardRepository);
            case CREATETEAM:
                return new CreateTeamCommand(taskBoardRepository);
            case REGISTERMEMBER:
                return new RegisterMemberCommand(taskBoardRepository);
            case SHOWMEMBERS:
                return new ShowMembersCommand(taskBoardRepository);
            case SHOWTEAMACTIVITY:
                return new ShowTeamActivityCommand(taskBoardRepository);
            case SHOWMEMBERACTIVITY:
                return new ShowMemberActivityCommand(taskBoardRepository);
            case LISTBUGS:
                return new ListBugsCommand(taskBoardRepository);
            case LISTTASKS:
                return new ListTasksCommand(taskBoardRepository);
            case LISTFEEDBACKS:
                return new ListFeedbacksCommand(taskBoardRepository);
            case LISTSTORIES:
                return new ListStoriesCommand(taskBoardRepository);
            case SHOWBOARDACTIVITY:
                return new ShowBoardActivityCommand(taskBoardRepository);
            case SHOWBOARDS:
                return new ShowBoardsCommand(taskBoardRepository);
            case CREATEBOARD:
                return new CreateBoardCommand(taskBoardRepository);          
            case ASSIGNBUG:
                return new AssignBugCommand(taskBoardRepository);
            case CHANGEBUGPRIORITY:
            return new ChangeBugPriorityCommand(taskBoardRepository);
            case CHANGEBUGEVERITY:
                return new ChangeBugServerityCommand(taskBoardRepository);
            case CHANGEBUGSTATUS:
                return new ChangeBugStatusCommand(taskBoardRepository);
            case CHANGESTORIPRIORITY:
                return new ChangeStoryPriorityCommand(taskBoardRepository);
            case CHANGESTORYSIZE:
                return new ChangeStorySizeCommand(taskBoardRepository);
            case CHANGESTORYSTATUS:
                return new ChangeStoryStatusCommand(taskBoardRepository);
            case CHANGEFEEDBACKSTATUS:
                return new ChangeFeedbackStatusCommand(taskBoardRepository);
            case CHANGEFEEDBACKRATING:
                return new ChangeFeedbackRatingCommand(taskBoardRepository);
            default:
                throw new IllegalArgumentException();
        }

//        return new CreateBugCommand(taskBoardRepository);
    }
}
