package com.telerikacademy.projectTeam08.core;

import com.telerikacademy.projectTeam08.core.contracts.TaskBoardRepository;
import com.telerikacademy.projectTeam08.models.BoardImpl;
import com.telerikacademy.projectTeam08.models.MemberImpl;
import com.telerikacademy.projectTeam08.models.TeamImpl;
import com.telerikacademy.projectTeam08.models.contracts.Board;
import com.telerikacademy.projectTeam08.models.contracts.Member;
import com.telerikacademy.projectTeam08.models.contracts.Team;
import com.telerikacademy.projectTeam08.models.tasks.BugImpl;
import com.telerikacademy.projectTeam08.models.tasks.CommentImpl;
import com.telerikacademy.projectTeam08.models.tasks.FeedbackImpl;
import com.telerikacademy.projectTeam08.models.tasks.StoryImpl;
import com.telerikacademy.projectTeam08.models.tasks.contracts.*;
import com.telerikacademy.projectTeam08.models.tasks.enums.*;
import com.telerikacademy.projectTeam08.models.tasks.EventLog;

import java.util.ArrayList;
import java.util.List;

public class TaskBoardRepositoryImpl implements TaskBoardRepository {


    private final static String NO_SUCH_MEMBER = "There is no member with name %s!";
    private static final String TEAM_ALREADY_EXISTS = "Team with %s name already exists!";
    private static final String NO_SUCH_TEAM = "There is no team with this name";
    public static final String BOARD_WITH_THIS_NAME_ALREADY_EXISTS = "Board with this name already exists";
    public static final String NO_BOARD_WITH_SUCH_NAME = "No board with such name.";
    public static final String NO_TASK_WITH_SUCH_ID = "No task with such id.";

    private int nextId;
//    private final List<Task> assignedBugs;
//    private final List<Task> bugList;

    private final List<Member> members;

    private final List<Board> boards;

    private final Member addedMember;

//    private final List<Task> storyList;
//
//    private final List<Task> feedbackList;

    private final List<Team> teams;

    private final List<Task> taskList;

    private Team createdTeam;

    public TaskBoardRepositoryImpl() {
        nextId = 0;
        taskList = new ArrayList<>();
//        bugList = new ArrayList<>();
//        storyList = new ArrayList<>();
//        feedbackList = new ArrayList<>();
//        assignedBugs = new ArrayList<>();
        this.teams = new ArrayList<>();
        this.members = new ArrayList<>();
        this.boards = new ArrayList<>();
        this.addedMember = null;
    }

    public List<Task> AssignBugs() {
        return new ArrayList<>(taskList);
    }
    @Override
    public List<Task> getBugs() {
        return new ArrayList<>(taskList);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    @Override
    public List<Task> getFeedbacks() {
        return new ArrayList<>(taskList);
    }

    @Override
    public List<Task> getStories() {
        return new ArrayList<>(taskList);
    }

    @Override
    public Bug createBug(String title, String description, PriorityType priorityType, SeverityType severityType, StatusBugType statusType, String assignee, List<String> steps) {
        Bug bug = new BugImpl(++nextId, title, description, priorityType, severityType, statusType, assignee, steps);

        taskList.add(bug);

        return bug;
    }

    @Override
    public Story createStory (String title, String description, PriorityType priorityType, SizeType sizeType, StatusStoryType statusStoryType, String assignee) {
        Story story = new StoryImpl(++nextId, title, description, priorityType, sizeType, statusStoryType, assignee);
        taskList.add(story);
        return story;

    }

    public Feedback createFeedback (String title, String description, int rating, StatusFeedbackType statusFeedbackType) {
        Feedback feedback = new FeedbackImpl(++nextId, title, description, rating, statusFeedbackType);
        taskList.add(feedback);
        return feedback;
    }

    @Override
    public Comment createComment(String content, String author) {
        return new CommentImpl(content, author);
    }

    @Override
    public List<Member> getMembers() {
        return new ArrayList<>(members);
    }

    @Override
    public void addMember(Member memberToAdd) {
        if (!members.contains(memberToAdd)) {
            this.members.add(memberToAdd);
        }
    }

    @Override
    public Member findMemberByName(String name) {
        Member member = members
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(NO_SUCH_MEMBER, name)));
        return member;
    }

    @Override
    public Member getAddedMember() {
        if (addedMember == null) {
            throw new IllegalArgumentException(NO_SUCH_MEMBER);
        }
        return addedMember;
    }

    @Override
    public Team createTeam (String name) {
        if(teamExists(name)) {
           throw new IllegalArgumentException(String.format(TEAM_ALREADY_EXISTS, name));
        }

        return new TeamImpl(name);
    }

    @Override
    public void addTeam(Team teamToAdd) {
        if(!teams.contains(teamToAdd)) {
            this.teams.add(teamToAdd);
        }
    }

    public List<Team> getTeams() {
        return new ArrayList<>(teams);
    }

    public boolean teamExists(String name) {
        return teams
                .stream()
                .anyMatch(t -> t.getName().equalsIgnoreCase(name));
    }

    public Team findTeamByName(String name) {
        return teams
                .stream()
                .filter(t -> t.getName().equalsIgnoreCase(name))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format(NO_SUCH_TEAM, name)));
    }

    //?
    @Override
    public List<EventLog> showTeamActivity(String name) {
        Team team = findTeamByName(name);

        return team.showActivity();
    }

    @Override
    public List<EventLog> showMemberActivity(String name) {
        Member member = findMemberByName(name);
        return member.showActivity();
    }

    @Override
    public Member createMember(String name) {
        return new MemberImpl(name);
    }

    @Override
    public Board createBoard(String name) {
        if(boardExists(name)){
            throw new IllegalArgumentException("Board with this name already exists");
        }
//            System.out.println("Board with this name already exists");

        Board board = new BoardImpl(name);

        boards.add(board);

        return board;
    }

    @Override
    public List<Board> showBoards() {
        return boards;
    }

    @Override
    public List<EventLog> showBoardActivity(String name) {
        Board board = findBoardByName(name);

        return board.showActivity();
    }

    public Task addTaskToBoard(int taskId, String boardName) {
        Board board = findBoardByName(boardName);
        Task task = findTaskById(taskId);

        board.addTask(task);
        return task;
    }

    @Override
    public List<Task> getTasks() {
        return new ArrayList<>(taskList);
    }


    public boolean boardExists(String name) {
        return boards
                .stream()
                .anyMatch(b -> b.getName().equalsIgnoreCase(name));
    }

    public Board findBoardByName(String name) {
        return boards
                .stream()
                .filter(b -> b.getName().equalsIgnoreCase(name))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format(NO_BOARD_WITH_SUCH_NAME, name)));
    }

    public Task findTaskById(int taskId) {
//        for(Task story : storyList) {
//            if(story.getId() == taskId)
//                return story;
//        }
//
//        for(Task feedback : feedbackList) {
//            if(feedback.getId() == taskId)
//                return feedback;
//        }
//
//        for(Task bug : bugList) {
//            if(bug.getId() == taskId)
//                return bug;
//        }

        for (Task task : taskList) {
            if(task.getId() == taskId) {
                return task;
            }
        }

        throw new IllegalArgumentException(NO_TASK_WITH_SUCH_ID);
    }


}
