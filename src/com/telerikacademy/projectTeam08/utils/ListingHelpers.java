package com.telerikacademy.projectTeam08.utils;

import com.telerikacademy.projectTeam08.models.contracts.Printable;
import com.telerikacademy.projectTeam08.models.tasks.contracts.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.telerikacademy.projectTeam08.core.TaskBoardEngineImpl.REPORT_SEPARATOR;

public class ListingHelpers {

    public static String elementsToString(List<? extends Printable> elements) {
        List<String> result = new ArrayList<>();
        for (Printable element : elements) {
            result.add(element.getAsString());
        }
        return String.join(REPORT_SEPARATOR + System.lineSeparator(), result).trim();
    }

    public static String bugsToString(List<Task> bugs) {
        return elementsToString(bugs);
    }

    public static String feedbacksToString(List<Task> feedbacks) {
        return elementsToString(feedbacks);
    }

    public static String storiesToString(List<Task> stories) {
        return elementsToString(stories);
    }

    public static String tasksToString(List<Task> tasks) {
        return elementsToString(tasks);
    }
}
